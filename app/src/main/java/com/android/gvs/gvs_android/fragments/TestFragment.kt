package com.android.gvs.gvs_android.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.android.gvs.gvs_android.App

import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.activities.ItemProfileActivity
import com.android.gvs.gvs_android.adapters.GenericFirebaseRecyclerAdapter
import com.android.gvs.gvs_android.adapters.ItemAdapter
import com.android.gvs.gvs_android.models.Item
import com.android.gvs.gvs_android.models.ShoppingCart
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_test.*


/*
    Usar este fragmento para hacer y deshacer cosas
    Aquí puede pasar lo que sea, no hay leyes
    La unica regla será que este fragmento nunca vea la luz pública
*/
class TestFragment : BaseFragment() {

    var cartItem: MenuItem? = null

    lateinit var adapter: ItemAdapter

    override val tituloFragmento: String
        get() = "TEST"

    override fun setListeners() {
        adapter.listener = object : ItemAdapter.ItemQuantityListener {
            override fun onItemQuantityChanged(item: Item, quantity: Int) {
                App.shoppingCart.items.removeAll { it.id == item.id }
                for (i in 1..quantity) {
                    App.shoppingCart.items.add(item)
                }
                db.collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS).document(App.user.id).set(App.shoppingCart)
            }
        }
    }

    override fun initViews() {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val query = db.collection(App.FIRESTORE_COLLECTION_ITEMS)

        val response = FirestoreRecyclerOptions.Builder<Item>()
            .setQuery(query, Item::class.java)
            .build()

        adapter = ItemAdapter(activity!!, App.shoppingCart, object : GenericFirebaseRecyclerAdapter.OnViewHolderClick {
            override fun onClick(view: View, position: Int) {
                val intent = Intent(context, ItemProfileActivity::class.java)
                val item = adapter.getItem(position)
                intent.putExtra(ItemProfileActivity.EXTRA_MEDICINE_PROFILE_MID, item.id)
                context?.startActivity(intent)
            }
        }, response)
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()



        pruebas.adapter = adapter
        pruebas.layoutManager = LinearLayoutManager(activity!!)

        /*db.collection(App.FIRESTORE_COLLECTION_ITEMS)
            .get()
            .addOnCompleteListener{task ->
                if (task.isSuccessful){
                    if (task.result != null){
                        task.result?.forEach { document ->
                            Log.d("FBASE", document.data.toString())
                        }
                    }
                }
            }*/
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_toolbar, menu)
        cartItem = menu?.findItem(R.id.mnu_cart)
    }

}
