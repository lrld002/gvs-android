package com.android.gvs.gvs_android.helpers

enum class OpenpayErrorCode(val code: Int) {

    /*

        +----------+--------+---------------------------------------------------------------+
        |  Código  |  HTTP  |  Causa                                                        |
        +----------+--------+---------------------------------------------------------------+
        |                                    GENERALES                                      |
        +----------+--------+---------------------------------------------------------------+
        |   1000   |  500   |  Ocurrió un error interno en el servidor de Openpay           |
        |----------|--------|---------------------------------------------------------------|
        |   1001   |  400   |  El formato de la petición no es JSON, los campos no          |
        |          |        |  tienen el formato correcto o la petición no tiene campos     |
        |          |        |  requeridos.                                                  |
        |----------|--------|---------------------------------------------------------------|
        |   1002   |  401   |  La llamada no está autenticada o la autenticación es         |
        |          |        |  incorrecta                                                   |
        |----------|--------|---------------------------------------------------------------|
        |   1003   |  422   |  La operación no se pudo completar por que el valor de uno    |
        |          |        |  o más de los parametros no es correcto.                      |
        |----------|--------|---------------------------------------------------------------|
        |   1004   |  503   |  Un servicio necesario para el procesamiento de la            |
        |          |        |  transacción no se encuentra disponible                       |
        |----------|--------|---------------------------------------------------------------|
        |   1005   |  404   |  Uno de los recursos requeridos no existe                     |
        |----------|--------|---------------------------------------------------------------|
        |   1006   |  409   |  Ya existe una transacción con el mismo ID de orden           |
        |----------|--------|---------------------------------------------------------------|
        |   1007   |  402   |  La transferencia de fondos entre una cuenta de banco o       |
        |          |        |  tarjeta y la cuenta de Openpay no fue aceptada               |
        |----------|--------|---------------------------------------------------------------|
        |   1008   |  423   |  Una de las cuentas requeridas en la petición se encuentra    |
        |          |        |  desactivada                                                  |
        |----------|--------|---------------------------------------------------------------|
        |   1009   |  413   |  El cuerpo de la petición es demasiado grande                 |
        |----------|--------|---------------------------------------------------------------|
        |   1010   |  403   |  Se está utilizando la llave pública para hacer una llamada   |
        |          |        |  que requiere la llave privada, o bien, se esta usando la     |
        |          |        |  llave privada desde JavaScript                               |
        +----------+--------+---------------------------------------------------------------+
        |                                  ALMACENAMIENTO                                   |
        +----------+--------+---------------------------------------------------------------+
        |   2001   |  409   |  La cuenta de banco con esta CLABE ya se encuentra registrada |
        |          |        |  en el cliente                                                |
        |----------|--------|---------------------------------------------------------------|
        |   2002   |  409   |  La tarjeta con este número ya se encuentra registrada        |
        |          |        |  en el cliente                                                |
        |----------|--------|---------------------------------------------------------------|
        |   2003   |  409   |  El cliente con este identificador externo ya existe          |
        |----------|--------|---------------------------------------------------------------|
        |   2004   |  422   |  El digito verificador del número de tarjeta es inválido de   |
        |          |        |  acuerdo al algoritmo Luhn                                    |
        |----------|--------|---------------------------------------------------------------|
        |   2005   |  400   |  La fecha de expiración de la tarjeta es anterior a la fecha  |
        |          |        |  actual                                                       |
        |----------|--------|---------------------------------------------------------------|
        |   2006   |  400   |  El código de seguridad de la tarjeta CVV2 no fue             |
        |          |        |  proporcionado                                                |
        |----------|--------|---------------------------------------------------------------|
        |   2007   |  412   |  El número de tarjeta es de prueba. Sólo para Sandbox         |
        |----------|--------|---------------------------------------------------------------|
        |   2008   |  412   |  La tarjeta consultada no es válida para puntos               |
        |----------|--------|---------------------------------------------------------------|
        |   2009   |  412   |  El código de seguridad de la tarjeta (CVV2) no es válido     |
        +----------+--------+---------------------------------------------------------------+
        |                                     TARJETAS                                      |
        +----------+--------+---------------------------------------------------------------+
        |   3001   |  402   |  La tarjeta fue declinada                                     |
        |----------|--------|---------------------------------------------------------------|
        |   3002   |  402   |  La tarjeta ha expirado                                       |
        |----------|--------|---------------------------------------------------------------|
        |   3003   |  402   |  La tarjeta no tiene fondos suficientes                       |
        |----------|--------|---------------------------------------------------------------|
        |   3004   |  402   |  La tarjeta ha sido identificada como robada                  |
        |----------|--------|---------------------------------------------------------------|
        |   3005   |  402   |  La tarjeta ha sido marcada como fraudulenta                  |
        |----------|--------|---------------------------------------------------------------|
        |   3006   |  412   |  La operación no está peritida para este cliente o esta       |
        |          |        |  transacción                                                  |
        |----------|--------|---------------------------------------------------------------|
        |   3008   |  412   |  La tarjeta no soporta transacciones en línea                 |
        |----------|--------|---------------------------------------------------------------|
        |   3009   |  402   |  La tarjeta fue reportada como perdida                        |
        |----------|--------|---------------------------------------------------------------|
        |   3010   |  402   |  El banco restringió la tarjeta                               |
        |----------|--------|---------------------------------------------------------------|
        |   3011   |  402   |  El banco ha solicitado que la tarjeta sea retenida.          |
        |          |        |  Contacte al banco                                            |
        |----------|--------|---------------------------------------------------------------|
        |   3012   |  412   |  Se requiere solicitar al banco autorización para realizar    |
        |          |        |  este pago                                                    |
        +----------+--------+---------------------------------------------------------------+
        |                                      CUENTAS                                      |
        +----------+--------+---------------------------------------------------------------+
        |   4001   |  412   |  La cuenta de Openpay no tiene fondos suficientes             |
        |----------|--------|---------------------------------------------------------------|
     */

    //GENERAL
    INTERNAL_SERVER_ERROR(1000), //HTTP 500
    BAD_REQUEST(1001), //HTTP 400
    UNAUTHORIZED(1002), //HTTP 401
    UNPROCESSABLE_ENTITY(1003), //HTTP 422
    SERVICE_UNAVAILABLE(1004), //HTTP 503
    NOT_FOUND(1005), //HTTP 404
    TRANSACTION_ID_ALREADY_EXISTS(1006), //HTTP 409
    PAYMENT_REQUIRED(1007), //HTTP 402
    LOCKED(1008), //HTTP 423
    REQUEST_ENTITY_TOO_LARGE(1009), //HTTP 413
    FORBIDDEN(1010), //HTTP 403

    //ALMACENAMIENTO
    CLABE_ALREADY_ON_CLIENT(2001), //HTTP 409
    CARD_NUMBER_ALREADY_ON_CLIENT(2002), //HTTP 409
    CLIENT_WITH_EXTERNAL_ID_ALREADY_EXISTS(2003), //HTTP 409
    INVALID_VERIFICATION_NUMBER(2004), //HTTP 422
    CARD_EXPIRATION_DATE_ALREADY_PASSED(2005), //HTTP 400
    CVV2_NOT_PROVIDED(2006), //HTTP 400
    SANDBOX_CARD_NUMBER_PROVIDED(2007), //HTTP 412
    QUERIED_CARD_NOT_VALID_FOR_POINTS(2008), //HTTP 412
    NOT_VALID_CVV2_CARD_NUMBER(2009), //HTTP 412

    //TARJETAS
    DECLINED_CARD(3001), //HTTP 402
    EXPIRED_CARD(3002), //HTTP 402
    CARD_DOESNT_HAVE_ENOUGH_FUNDS(3003), //HTTP 402
    CARD_MARKED_AS_STOLEN(3004), //HTTP 402
    CHEAT_CARD(3005), //HTTP 402
    OPERATION_NOT_ALLOWED_FOR_CLIENT_OR_TRANSACTION(3006), //HTTP 412
    CARD_DONT_SUPPORT_ONLINE_TRANSACTIONS(3008), //HTTP 412
    LOST_CARD(3009), //HTTP 402
    BANK_RESTRICTED_THE_CARD(3010), //HTTP 402
    BANK_ASKED_TO_RETAIN_THE_CARD(3011), //HTTP 402
    BANK_AUTHORIZATION_NEEDED_TO_PAY(3012), //HTTP 412

    //CUENTAS
    OPENPAY_ACCOUNT_NOT_ENOUGH_FUNDS(4001), //HTTP 412


}