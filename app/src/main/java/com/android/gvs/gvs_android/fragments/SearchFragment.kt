package com.android.gvs.gvs_android.fragments

import android.app.SearchManager
import android.content.Context.SEARCH_SERVICE
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.activities.ItemProfileActivity
import com.android.gvs.gvs_android.adapters.GenericFirebaseRecyclerAdapter
import com.android.gvs.gvs_android.adapters.ItemAdapter
import com.android.gvs.gvs_android.adapters.ItemSearchResultsAdapter
import com.android.gvs.gvs_android.models.Item
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * Created by SGIFER on 07/11/18.
 */

class SearchFragment : BaseFragment(), ItemAdapter.ItemQuantityListener {

    var searchItem: MenuItem? = null
    var searchView: SearchView? = null
    lateinit var queryTextListener: SearchView.OnQueryTextListener
    lateinit var searchManager: SearchManager
    lateinit var adapter: ItemSearchResultsAdapter

    lateinit var searchQuery: Query
    lateinit var response: FirestoreRecyclerOptions<Item>

    companion object {
        val EXTRA_PROFILE_MID = "XTRA_PRMID"
    }

    override val tituloFragmento: String
        get() = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_search, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_search, menu)

        searchItem = menu?.findItem(R.id.mnu_search)
        searchManager = activity?.getSystemService(SEARCH_SERVICE) as SearchManager

        if (searchItem != null) {
            searchView = searchItem?.actionView as SearchView?
            searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            queryTextListener = object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(query: String): Boolean {
                    search(query)
                    return true
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    search(query)
                    return true
                }
            }

            searchView?.setOnQueryTextListener(queryTextListener)
            searchView?.queryHint = getString(R.string.search_medicine)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
    }

    override fun initViews() {
        medicine_search_list.layoutManager = LinearLayoutManager(context)
        searchQuery = db.collection(App.FIRESTORE_COLLECTION_ITEMS)
        showAdapter(searchQuery, "")
    }

    override fun setListeners() {

    }

    private fun search(query: String) {
        searchQuery = db.collection(App.FIRESTORE_COLLECTION_ITEMS)
        showAdapter(searchQuery, query)
    }

    private fun showAdapter(query: Query, text: String) {
        query.get()
            .addOnCompleteListener {
                val items = ArrayList<Item>()
                if (it.isSuccessful) {
                    it.result?.documents?.forEach { doc ->
                        val item = doc.toObject(Item::class.java)
                        if (item != null)
                            if (text.isEmpty()) {
                                items.add(item)
                            } else if (item.name.toLowerCase().contains(text.toLowerCase()))
                                items.add(item)
                    }
                    adapter = ItemSearchResultsAdapter(activity!!, null)
                    adapter.quantityListener = this
                    adapter.list = items
                    medicine_search_list.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
            }
    }

    override fun onItemQuantityChanged(item: Item, quantity: Int) {
        App.shoppingCart.items.removeAll { it.id == item.id }
        for (i in 1..quantity) {
            App.shoppingCart.items.add(item)
        }
        db.collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS).document(App.user.id).set(App.shoppingCart)

    }
}