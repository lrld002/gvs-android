package com.android.gvs.gvs_android.models

/**
 * Created by SGIFER on 09/11/18.
 */

class User {
    lateinit var id: String
    lateinit var mail: String
    lateinit var name: String
    lateinit var photoUrl: String
    var gender: Int = 0
    var timeStamp: Long = 0L
    //var collaborates: Boolean? = null
    //var text: String? = null

    constructor() {

    }

    constructor(id: String, mail: String, name: String, photoUrl: String, gender: Int, timeStamp: Long, collaborates: Boolean) {
        this.id = id
        this.mail = mail
        this.name = name
        this.photoUrl = photoUrl
        this.gender = gender
        this.timeStamp = timeStamp
        //this.collaborates = collaborates
    }
}