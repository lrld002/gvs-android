package com.android.gvs.gvs_android.models

class Item {
    var id: String = ""
    var name: String = ""
    var service: Boolean = false
    var price: Double = 0.0
    var text: String = ""
    var department: String = ""
    var category: List<String> = ArrayList()
    var prescription: Boolean = false
    var tax: Boolean = false
    var images: List<String> = ArrayList()

    constructor() {

    }

    constructor(
        id: String,
        name: String,
        service: Boolean,
        price: Double,
        text: String,
        department: String,
        category: ArrayList<String>,
        prescription: Boolean,
        tax: Boolean,
        images: List<String>
    ) {
        this.id = id
        this.name = name
        this.service = service
        this.price = price
        this.text = text
        this.department = department
        this.category = category
        this.prescription = prescription
        this.tax = tax
        this.images = images
    }
}