package com.android.gvs.gvs_android.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.models.User
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity() {


    companion object {
        const val TAG = "GVS_LOGIN"
    }

    private lateinit var mAuth: FirebaseAuth

    private lateinit var mCallbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        mAuth = FirebaseAuth.getInstance()
        mCallbackManager = CallbackManager.Factory.create()

        initViews()
        setListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mCallbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun initViews() {
        signup_button_facebook.setReadPermissions("email", "public_profile")
    }

    override fun setListeners() {

        signup_button_facebook.registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d(SignUpActivity.TAG, "facebook:onSuccess:" + result)
                if (result != null) {
                    handleFacebookAccessToken(result.accessToken)
                }
            }

            override fun onCancel() {
                Log.d(SignUpActivity.TAG, "facebook:onCancel")
            }

            override fun onError(error: FacebookException?) {
                Log.d(SignUpActivity.TAG, "facebook:onError", error)

            }
        })
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(SignUpActivity.TAG, "handleFacebookAccessToken:" + token)

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) {
                Log.d(SignUpActivity.TAG, "signInWithCredentia:success")
            }
            .addOnSuccessListener(this) { authResult ->
                if (authResult != null) {
                    val firebaseUser = authResult.user
                    var facebookId = ""
                    firebaseUser.providerData
                        .filter { it.providerId == FacebookAuthProvider.PROVIDER_ID }
                        .forEach { facebookId = it.uid }
                    val photoUrl = "https://graph.facebook.com/$facebookId/picture?width=1024&height=1024"
                    val gender: Int = 0
                    val birthDate2: Long = 0

                    val user = User(
                        firebaseUser.uid,
                        firebaseUser.email.orEmpty(),
                        firebaseUser.displayName.orEmpty(),
                        photoUrl,
                        gender,
                        birthDate2,
                        false
                    )

                    db.collection(App.FIRESTORE_COLLECTION_USERS)
                        .document(user.id)
                        .set(user)
                        .addOnSuccessListener { }
                        .addOnFailureListener { }

                }
            }
            .addOnFailureListener(this) { ex ->
                if (ex is FirebaseAuthUserCollisionException) {
                    LoginManager.getInstance().logOut()
                    val ad = AlertDialog.Builder(this@SignUpActivity)
                    ad.setMessage(getString(R.string.auth_user_collision))
                    ad.setPositiveButton(getString(R.string.ok), null)
                    ad.show()
                } else {
                    ex.printStackTrace()
                }
            }
    }

}
