package com.android.gvs.gvs_android.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.fragments.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.item_home -> {
                startFragmentTransaction(TestFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.item_search -> {
                startFragmentTransaction(SearchFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.item_profile -> {
                startFragmentTransaction(ProfileFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setListeners()
        startFragmentTransaction(TestFragment())

    }


    private fun startFragmentTransaction(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.FragmentContainer, fragment)
            .commit()
    }



    override fun initViews() {
        main_cart_button.setOnClickListener {
            startFragmentTransaction(CartFragment())
        }

    }

    override fun setListeners() {
        navigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

}
