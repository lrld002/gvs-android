package com.android.gvs.gvs_android.fragments

import android.support.v4.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.google.firebase.firestore.FirebaseFirestore

abstract class BaseFragment : Fragment() {

    val db = FirebaseFirestore.getInstance()

    abstract val tituloFragmento: String

    protected abstract fun setListeners()

    protected abstract fun initViews()
}
