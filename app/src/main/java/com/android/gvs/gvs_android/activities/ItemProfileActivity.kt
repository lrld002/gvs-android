package com.android.gvs.gvs_android.activities

import android.os.Bundle
import android.widget.Toast
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.models.Item
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.activity_medicine_profile.*

/**
 * Created by SGIFER on 07/11/18.
 */

class ItemProfileActivity : BaseActivity() {

    var currentItem: Item? = null
    lateinit var glide: RequestManager

    companion object {
        val EXTRA_MEDICINE_PROFILE_MID = "XTRA_IID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medicine_profile)
        glide = Glide.with(this)

        db.collection(App.FIRESTORE_COLLECTION_ITEMS)
            .document(intent?.extras?.getString(EXTRA_MEDICINE_PROFILE_MID) ?: "")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    currentItem = document?.toObject(Item::class.java)
                    fillWithDocument()
                }
            }

        initViews()
        setListeners()
    }


    override fun initViews() {

    }

    override fun setListeners() {
        val item= Item()
        add_to_cart_medicine_profile.setOnClickListener {
            App.shoppingCart.items.add(item)
            Toast.makeText(this,"Se agregó al carrito",Toast.LENGTH_LONG).show()
        }
    }

    private fun fillWithDocument() {
        itemName.text = currentItem?.name
        itemPrice.text = currentItem?.price.toString()
        itemText.text = currentItem?.text
        if (currentItem != null && currentItem?.images != null)
            glide.load(if (currentItem!!.images.isEmpty())
                App.GVS_DEFAULT_MEDICINE_IMAGE_URL else currentItem?.images?.firstOrNull { it.isNotBlank() || it.isNotEmpty() })
                .into(
                    itemImage
                )
    }

}