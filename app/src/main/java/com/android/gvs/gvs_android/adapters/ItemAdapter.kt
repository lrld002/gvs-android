package com.android.gvs.gvs_android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.models.Item
import com.android.gvs.gvs_android.models.ShoppingCart
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.firebase.ui.common.ChangeEventType
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentSnapshot

class ItemAdapter(
    context: Context,
    var shoppingCart: ShoppingCart,
    listener: GenericFirebaseRecyclerAdapter.OnViewHolderClick,
    response: FirestoreRecyclerOptions<Item>
) : GenericFirebaseRecyclerAdapter<Item>(context, listener, response) {

    interface ItemQuantityListener {
        fun onItemQuantityChanged(item: Item, quantity: Int)
    }

    private val glide: RequestManager = Glide.with(context)

    private val map = HashMap<String, Int>()

    var listener: ItemQuantityListener? = null

    override fun createView(context: Context, viewGroup: ViewGroup, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.layout_item, viewGroup, false)
    }

    override fun bindView(item: Item, position: Int, viewHolder: ViewHolder) {
        val itemImage = viewHolder.getView(R.id.itemImage) as ImageView
        val itemName = viewHolder.getView(R.id.itemName) as TextView
        val itemPrice = viewHolder.getView(R.id.itemPrice) as TextView
        val setQuantity = viewHolder.getView(R.id.setQuantity) as ImageView
        val addToCart = viewHolder.getView(R.id.addToCart) as ImageView
        val layoutItemQty = viewHolder.getView(R.id.layoutItemQty) as LinearLayout
        val itemQty = viewHolder.getView(R.id.itemQty) as TextView
        val itemQtyMinus = viewHolder.getView(R.id.itemQtyMinus) as TextView
        val itemQtyPlus = viewHolder.getView(R.id.itemQtyPlus) as TextView

        setQuantity.visibility = View.VISIBLE
        addToCart.visibility = View.GONE
        layoutItemQty.visibility = addToCart.visibility

        setQuantity.setOnClickListener {
            setQuantity.visibility = View.GONE
            addToCart.visibility = View.VISIBLE
            layoutItemQty.visibility = addToCart.visibility
        }

        itemName.text = item.name

        itemQty.text = if (map.containsKey(item.id)) map.getValue(item.id).toString() else "1"
        //itemPrice.text = item.price.toString()
        itemPrice.text = "$" + item.price.toString()

        glide.load(if (item.images.isEmpty())
            App.GVS_DEFAULT_MEDICINE_IMAGE_URL else item.images.firstOrNull { it.isNotBlank() || it.isNotEmpty() })
            .into(
                itemImage
            )

        itemQtyMinus.setOnClickListener {
            var qty = itemQty.text.toString().toInt()
            if (qty == 1)
                return@setOnClickListener
            map[item.id] = --qty
            itemQty.text = map.getValue(item.id).toString()
        }

        itemQtyPlus.setOnClickListener {
            var qty = itemQty.text.toString().toInt()
            map[item.id] = ++qty
            itemQty.text = map.getValue(item.id).toString()
        }

        addToCart.setOnClickListener {
            val qty = itemQty.text.toString().toInt()
            listener?.onItemQuantityChanged(item, qty)
        }
    }

    override fun onChildChanged(type: ChangeEventType, snapshot: DocumentSnapshot, newIndex: Int, oldIndex: Int) {
        super.onChildChanged(type, snapshot, newIndex, oldIndex)
        val item = snapshot.toObject(Item::class.java)
        if (item != null) {
            when (type) {
                ChangeEventType.ADDED -> {
                    map[item.id] = 1
                }
                ChangeEventType.CHANGED -> {

                }
                ChangeEventType.MOVED -> {
                }
                ChangeEventType.REMOVED -> {
                    map.remove(item.id)
                }

            }
        }
    }
}