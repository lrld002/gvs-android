package com.android.gvs.gvs_android.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.models.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

/**
 * Created by SGIFER on 08/11/18.
 */

class SplashActivity : BaseActivity() {

    //private lateinit var gvsLogo : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_splash)

        initViews()
        setListeners()

        if (FirebaseAuth.getInstance().currentUser != null) {
            nextActivity()
        } else {
            FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val firebaseUser = FirebaseAuth.getInstance().currentUser
                        if (firebaseUser != null) {
                            val user = User(
                                firebaseUser.uid,
                                firebaseUser.email.orEmpty(),
                                "Anon",
                                "",
                                -1,
                                0,
                                false
                            )
                            db.collection(App.FIRESTORE_COLLECTION_USERS)
                                .document(user.id)
                                .set(user)
                                .addOnSuccessListener {
                                    nextActivity()
                                }
                                .addOnFailureListener { }
                        }
                    }
                }

        }
        finish()

    }

    private fun nextActivity() {
        val intent = Intent(this@SplashActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun initViews() {
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    override fun setListeners() {
    }
}