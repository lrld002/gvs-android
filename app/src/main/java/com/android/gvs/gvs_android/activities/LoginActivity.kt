package com.android.gvs.gvs_android.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.android.gvs.gvs_android.R

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initViews()
        setListeners()
    }

    override fun setListeners() {

    }

    override fun initViews() {

    }
}
