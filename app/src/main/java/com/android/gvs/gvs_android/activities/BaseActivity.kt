package com.android.gvs.gvs_android.activities

import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.google.firebase.firestore.FirebaseFirestore

abstract class BaseActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    internal abstract fun initViews()

    internal abstract fun setListeners()

}
