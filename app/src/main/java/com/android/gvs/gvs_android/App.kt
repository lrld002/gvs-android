package com.android.gvs.gvs_android

import android.app.Application
import com.android.gvs.gvs_android.models.ShoppingCart
import com.android.gvs.gvs_android.models.User
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.HashMap


/**
 * Created by SGIFER on 07/11/18.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null) {
            FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_USERS)
                .document(firebaseUser.uid)
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        user = it.result?.toObject(User::class.java) ?: User()
                        FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_SHOPPING_CARTS)
                            .whereEqualTo("id", user.id)
                            .limit(1)
                            .get()
                            .addOnCompleteListener {task ->
                                if (task.isSuccessful){
                                    if (task.result != null){
                                        if (!task.result!!.isEmpty){
                                            shoppingCart = task.result!!.first().toObject(ShoppingCart::class.java)
                                        }else{
                                            shoppingCart = ShoppingCart(user.id , user, ArrayList())
                                        }
                                    }else{
                                        shoppingCart = ShoppingCart(user.id , user, ArrayList())
                                    }
                                }

                            }
                    }
                }

        }

        //currentDatabase = FirebaseDatabase.getInstance()

        //database_table_users = currentDatabase.getReference(FIREBASE_DATABASE_TABLE_USERS)
        //database_table_medicines = currentDatabase.getReference(FIREBASE_DATABASE_TABLE_MEDICINES)

        //database_table_users.keepSynced(true)
        //database_table_medicines.keepSynced(true)

    }

    companion object {

        /*fun getUser(): User {
            val firebaseUser = FirebaseAuth.getInstance().currentUser
            if (firebaseUser != null) {
                return FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_USERS)
                    .document(firebaseUser.uid)
                    .get()
                    .result?.toObject(User::class.java) ?: User()
            }
            return User()
        }*/

        fun getCart(user: User): ShoppingCart {
            return FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_SHOPPING_CARTS)
                .whereEqualTo("id", user.id)
                .limit(1)
                .get()
                .result?.first()?.toObject(ShoppingCart::class.java) ?: ShoppingCart(user.id, user, ArrayList())
        }

        val TAG = "GVS"

        var user: User = User()
        var shoppingCart: ShoppingCart = ShoppingCart()

        lateinit var currentAuth: FirebaseAuth
        //lateinit var currentDatabase: FirebaseDatabase

        //lateinit var database_table_users: DatabaseReference
        //lateinit var database_table_medicines: DatabaseReference

        val GVS_DEFAULT_MEDICINE_IMAGE_URL =
            "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fproximamente.jpeg?alt=media&token=c5f06878-0809-4453-9230-e3360dfa1b4c"

        val FIRESTORE_COLLECTION_ITEMS = "items"
        val FIRESTORE_COLLECTION_USERS = "users"
        val FIRESTORE_COLLECTION_SHOPPING_CARTS = "shoppingCarts"

        val FIREBASE_DATABASE_TABLE_MEDICINES = "medicines"

    }

}