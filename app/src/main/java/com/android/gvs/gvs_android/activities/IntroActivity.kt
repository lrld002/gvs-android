package com.android.gvs.gvs_android.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.models.User
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.auth.api.Auth
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_intro.*

/**
 * Created by SGIFER on 08/11/18.
 */

class IntroActivity : BaseActivity() {

    companion object {
        val RC_SIGN_IN = 0x01
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        initViews()
        setListeners()
    }

    override fun initViews() {
    }

    override fun setListeners() {
        loginButton.setOnClickListener {
            val providers = arrayListOf(
                AuthUI.IdpConfig.EmailBuilder().build(),
                AuthUI.IdpConfig.FacebookBuilder().build()
            )
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setLogo(R.drawable.logo_gvs_version_2)
                    .setTheme(R.style.AppTheme)
                    .build(),
                RC_SIGN_IN
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val firebaseUser = FirebaseAuth.getInstance().currentUser
                if (firebaseUser != null) {
                    var facebookId = ""

                    var photoUrl = ""

                    firebaseUser.providerData
                        .forEach {
                            when (it.providerId) {
                                FacebookAuthProvider.PROVIDER_ID -> {
                                    facebookId = it.uid
                                    photoUrl = "https://graph.facebook.com/$facebookId/picture?width=1024&height=1024"
                                }
                                EmailAuthProvider.PROVIDER_ID -> {
                                    photoUrl = ""
                                }
                            }
                        }
                    val gender: Int = 0
                    val birthDate2: Long = 0

                    val user = User(
                        firebaseUser.uid,
                        firebaseUser.email.orEmpty(),
                        firebaseUser.displayName.orEmpty(),
                        photoUrl,
                        gender,
                        birthDate2,
                        false
                    )


                    db.collection(App.FIRESTORE_COLLECTION_USERS)
                        .document(user.id)
                        .set(user)
                        .addOnSuccessListener { }
                        .addOnFailureListener { }
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

}