package com.android.gvs.gvs_android.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.gvs.gvs_android.App
import com.android.gvs.gvs_android.R
import com.android.gvs.gvs_android.activities.CheckoutActivity
import com.android.gvs.gvs_android.adapters.BaseRecyclerViewAdapter
import com.android.gvs.gvs_android.adapters.ShoppingCartAdapter
import com.android.gvs.gvs_android.models.Item
import kotlinx.android.synthetic.main.fragment_cart.*

/**
 * Created by SGIFER on 07/11/18.
 */

class CartFragment : BaseFragment() {

    lateinit var adapter: ShoppingCartAdapter

    override val tituloFragmento: String
        get() = ""

    override fun setListeners() {
        checkout_button.setOnClickListener {
            val intent = Intent(this.context, CheckoutActivity::class.java)
            startActivity(intent)
        }
    }

    override fun initViews() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //val query = db
        //    .collection(App.FIRESTORE_COLLECTION_ITEMS)

        adapter = ShoppingCartAdapter(activity!!, object : BaseRecyclerViewAdapter.OnViewHolderClick<Item> {
            override fun onClick(view: View, position: Int, item: Item?) {

            }
        })

        return inflater.inflate(R.layout.fragment_cart, container, false)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
        cesto.adapter = adapter
        cesto.layoutManager = LinearLayoutManager(activity!!)

        adapter.list = App.shoppingCart.items
        adapter.notifyDataSetChanged()
    }
}